import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart' as firebase_core;

class StorageHandler {
  Future<String> uploadFile(File file, String id) async {
    // firebase_storage.FirebaseStorage storage =
    //     firebase_storage.FirebaseStorage.instance;
    try {
      print('Passed file path ${file.path}');
      //When you want to use task snapshot directly
      firebase_storage.UploadTask task = firebase_storage
          .FirebaseStorage.instance
          .ref('users/$id.${getImageExtension(file.path)}')
          .putFile(file);
      // When you want to use task as stream to handle it in UI
      task.asStream().listen((event) {
        print(
            "Current task state ${((event.bytesTransferred / event.totalBytes) * 100).toStringAsFixed(2)}");
      });
      firebase_storage.TaskSnapshot taskSnapshot = await task;
      String uploadedFileUrl = await taskSnapshot.ref.getDownloadURL();
      print("Downloaded file path: $uploadedFileUrl");
      return uploadedFileUrl;
    } on firebase_core.FirebaseException catch (e) {
      throw e.message;
    }
  }

  String getImageExtension(String filePath) {
    String fileName = filePath.split("/").last;
    String fileExtension = fileName.split(".").last;
    return fileExtension;
  }
}
