import 'package:iti_smart_app/models/httpError.dart';
import 'package:iti_smart_app/utils/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ServicesHandler {
  Future<dynamic> httpGet(String endPoint,
      {Map<String, String> headers}) async {
    Uri uri = Uri.parse('${Constants.BASE_HOST}$endPoint');
    Map<String, String> requestHeaders = {};
    if (headers != null) {
      requestHeaders = headers;
    } else {
      requestHeaders = {'Content-Type': 'application/json'};
    }
    http.Response response = await http.get(uri, headers: requestHeaders);
    if (response.statusCode == 200 || response.statusCode == 201) {
      return jsonDecode(response.body);
    } else if (response.statusCode == 400) {
      HttpError error = HttpError.fromJson(jsonDecode(response.body));
      throw error;
    } else if (response.statusCode >= 500) {
      throw Exception("Something wrong happened!");
    }
  }
}

/*
request:
url,
method,
headers,
body

response:
status code,
headers,
body (String)
*/
