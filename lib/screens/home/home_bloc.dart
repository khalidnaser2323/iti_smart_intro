import 'package:bloc/bloc.dart';

//event, eventToState function, state emit, ui
enum HomeBlocEvent { incrementCounter, decrementCounter }

class HomeState {
  int counter;
  HomeState({this.counter});
}

class HomeBloc extends Bloc<HomeBlocEvent, HomeState> {
  HomeBloc() : super(HomeState(counter: 10));
  @override
  Stream<HomeState> mapEventToState(HomeBlocEvent event) async* {
    if (event == HomeBlocEvent.incrementCounter) {
      HomeState newState = HomeState();
      newState.counter = state.counter + 1;
      yield newState;
    }
    if (event == HomeBlocEvent.decrementCounter) {
      HomeState newState = HomeState();
      newState.counter = state.counter - 1;
      yield newState;
    }
  }
}
/*

let observable;
observable.emit(2);
observable.subscibe((data){
  console.log(data);// 2
});
 */
