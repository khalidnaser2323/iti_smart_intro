import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iti_smart_app/screens/home/home_bloc.dart';

class HomeView extends StatelessWidget {
  final String appTitle;
  final HomeBloc _homeBloc = HomeBloc();
  HomeView({@required this.appTitle});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: _getBody(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _getFloatingButton(),
    );
  }

  Widget _getAppBar() {
    return AppBar(
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.menu,
        ),
        onPressed: () {
          print("Menu icon is clicked");
        },
      ),
      title: Text(
        appTitle,
      ),
    );
  }

  Widget _getBody() {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("State count is"),
          BlocBuilder<HomeBloc, HomeState>(
              cubit: _homeBloc,
              builder: (context, state) {
                return Text("${state.counter}");
              }),
        ],
      ),
    );
  }

  Widget _getFloatingButton() {
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        _homeBloc.add(HomeBlocEvent.incrementCounter);
      },
    );
  }
}
/*
class HomeView extends StatefulWidget {
  final String appTitle;
  HomeView({@required this.appTitle});
  @override
  State<StatefulWidget> createState() {
    return _HomeViewState();
  }
}

class _HomeViewState extends State<HomeView> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: _getBody(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _getFloatingButton(),
    );
  }

  Widget _getAppBar() {
    return AppBar(
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.menu,
        ),
        onPressed: () {
          print("Menu icon is clicked");
        },
      ),
      title: Text(
        widget.appTitle,
      ),
    );
  }

  Widget _getBody() {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("State count is"),
          Text("$_counter"),
        ],
      ),
    );
    // return Container(
    //   color: Colors.blue,
    //   height: 100.0,
    //   child: Row(
    //     mainAxisSize: MainAxisSize.max,
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     crossAxisAlignment: CrossAxisAlignment.center,
    //     children: [
    //       Expanded(
    //         flex: 2,
    //         child: Container(
    //           color: Colors.black,
    //           width: 50.0,
    //           height: 50.0,
    //         ),
    //       ),
    //       Expanded(
    //         flex: 2,
    //         child: Container(
    //           color: Colors.white,
    //           width: 50.0,
    //           height: 50.0,
    //         ),
    //       ),
    //       Expanded(
    //         flex: 2,
    //         child: Container(
    //           color: Colors.red,
    //           width: 50.0,
    //           height: 50.0,
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }

  Widget _getFloatingButton() {
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        setState(() {
          _counter++;
        });
      },
    );
  }
}
*/
