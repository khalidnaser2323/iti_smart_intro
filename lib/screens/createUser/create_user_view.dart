import 'package:flutter/material.dart';
import 'package:iti_smart_app/models/user.dart';

class CreateUserView extends StatefulWidget {
  @override
  _CreateUserViewState createState() => _CreateUserViewState();
}

class _CreateUserViewState extends State<CreateUserView> {
  TextEditingController _nameFieldController;
  String _name, _email, _image, _id;
  final _formState = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _nameFieldController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create new user"),
      ),
      body: _getCreateUserForm(),
    );
  }

  Widget _getCreateUserForm() {
    return Padding(
      padding: EdgeInsets.all(12),
      child: Form(
          key: _formState,
          child: Column(
            children: [
              _getIdField(),
              _getGetSpace(),
              _getNameField(),
              _getGetSpace(),
              _getEmailField(),
              _getGetSpace(),
              _getImageField(),
              _getGetSpace(),
              _getSubmitButton(),
            ],
          )),
    );
  }

  Widget _getGetSpace() {
    return SizedBox(
      height: 10.0,
    );
  }

  Widget _getIdField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.done,
      validator: (String value) {
        if (value.isEmpty) {
          return "Please enter your id";
        } else {
          return null;
        }
      },
      onSaved: (String value) {
        print("Save function called $value");
        _id = value;
      },
      decoration: _getGenericInputDecoration("ID"),
    );
  }

  Widget _getNameField() {
    return TextFormField(
      controller: _nameFieldController,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.done,
      validator: (String value) {
        if (value.isEmpty) {
          return "Please enter your name";
        } else {
          return null;
        }
      },
      onSaved: (String value) {
        _name = value;
      },
      decoration: _getGenericInputDecoration("Name"),
    );
  }

  Widget _getEmailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.done,
      validator: (String value) {
        if (value.isEmpty) {
          return "Please enter your email";
        } else if (!value.contains("@")) {
          return "Please enter valid email";
        } else {
          return null;
        }
      },
      onSaved: (String value) {
        _email = value;
      },
      decoration: _getGenericInputDecoration("Email"),
    );
  }

  Widget _getImageField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.done,
      validator: (String value) {
        if (value.isEmpty) {
          return "Please enter your image";
        } else {
          return null;
        }
      },
      onSaved: (String value) {
        _image = value;
      },
      decoration: _getGenericInputDecoration("Image"),
    );
  }

  Widget _getSubmitButton() {
    return RaisedButton(
      child: Text("Submit"),
      onPressed: () {
        if (_formState.currentState.validate()) {
          _formState.currentState.save();
          User newUser = User(
              id: int.parse(_id), name: _name, image: _image, email: _email);
          print("New created user");
          print(newUser);
          Navigator.of(context).pop(newUser);
        }
      },
    );
  }

  InputDecoration _getGenericInputDecoration(String labelText) {
    return InputDecoration(
      labelText: labelText,
      focusedBorder: OutlineInputBorder(),
      enabledBorder: OutlineInputBorder(),
      border: OutlineInputBorder(),
    );
  }
}
