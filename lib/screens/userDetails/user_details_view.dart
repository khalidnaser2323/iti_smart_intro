import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iti_smart_app/components/user_card.dart';
import 'package:iti_smart_app/models/general_state.dart';
import 'package:iti_smart_app/models/user.dart';
import 'package:iti_smart_app/screens/users/users_list_bloc.dart';
import 'package:iti_smart_app/screens/users/users_view_model.dart';

class UserDetailsView extends StatefulWidget {
  UserDetailsView();
  @override
  _UserDetailsViewState createState() => _UserDetailsViewState();
}

class _UserDetailsViewState extends State<UserDetailsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User details"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            //This is how to return data to previous view
            UsersListBloc selectedUserBloc =
                BlocProvider.of<UsersListBloc>(context);
            User user = selectedUserBloc.state.data.selectedUser;
            Navigator.of(context).pop(user);
          },
        ),
      ),
      body: Center(
        child: Container(
          height: 200.0,
          width: 200.0,
          child: BlocBuilder<UsersListBloc, GeneralState<UsersListState>>(
              builder: (context, state) {
            if (state.hasData && state.data.selectedUser != null) {
              return UserCard(
                displayAsCard: true,
                user: state.data.selectedUser,
              );
            } else {
              return Container();
            }
          }),
        ),
      ),
    );
  }
}

/*
class UserDetailsView extends StatelessWidget {
  final User user;
  UserDetailsView({this.user});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User details"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            //This is how to return data to previous view
            Navigator.of(context).pop(this.user);
          },
        ),
      ),
      body: Center(
        child: Container(
          height: 200.0,
          width: 200.0,
          child: UserCard(
            displayAsCard: true,
            user: user,
          ),
        ),
      ),
    );
  }
}
*/
