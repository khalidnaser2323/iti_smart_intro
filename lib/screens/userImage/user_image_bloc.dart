import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:iti_smart_app/handlers/storage_handler.dart';
import 'package:iti_smart_app/models/general_state.dart';

enum UserImageBlocEventType { uploadImage }

class UserImageEvent {
  UserImageBlocEventType type;
  File uploadedImage;
  String userId;
  UserImageEvent({this.type, this.uploadedImage, this.userId});
}

class UserImageState {
  String url;
  UserImageState({this.url});
}

class UserImageBloc extends Bloc<UserImageEvent, GeneralState<UserImageState>> {
  UserImageBloc() : super(GeneralState());
  @override
  Stream<GeneralState<UserImageState>> mapEventToState(
      UserImageEvent event) async* {
    try {
      print("Event");
      if (event.type == UserImageBlocEventType.uploadImage) {
        yield GeneralState(waiting: true);
        String url = await StorageHandler()
            .uploadFile(event.uploadedImage, event.userId);
        yield GeneralState(hasData: true, data: UserImageState(url: url));
      }
    } catch (error) {
      yield GeneralState(hasError: true, error: error.toString());
    }
  }
}
