import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iti_smart_app/models/general_state.dart';
import 'package:iti_smart_app/screens/userImage/user_image_bloc.dart';
import 'package:image_picker/image_picker.dart';

class UserImageView extends StatefulWidget {
  @override
  _UserImageViewState createState() => _UserImageViewState();
}

class _UserImageViewState extends State<UserImageView> {
  UserImageBloc userImageBloc;

  Future getImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.getImage(
      source: ImageSource.gallery,
      imageQuality: 30,
      maxWidth: 200.0,
      maxHeight: 200.0,
    );
    print("After pick file $pickedFile");
    setState(() {
      if (pickedFile != null) {
        File image = File(pickedFile.path);
        userImageBloc.add(UserImageEvent(
          uploadedImage: image,
          userId: '1',
          type: UserImageBlocEventType.uploadImage,
        ));
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    super.initState();
    userImageBloc = UserImageBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User Image"),
      ),
      body: Center(
        child: Column(
          children: [
            uploadImageButton(),
            SizedBox(
              height: 20.0,
            ),
            userImage(),
          ],
        ),
      ),
    );
  }

  Widget uploadImageButton() {
    return RaisedButton(
      child: Text("Upload Image"),
      onPressed: getImage,
    );
  }

  Widget userImage() {
    return BlocBuilder<UserImageBloc, GeneralState<UserImageState>>(
      cubit: userImageBloc,
      builder: (context, state) {
        if (state.hasData) {
          return Image.network(state.data.url);
        } else if (state.waiting) {
          return CircularProgressIndicator();
        } else if (state.hasError) {
          return Text(state.error);
        } else {
          return Container();
        }
      },
    );
  }
}
