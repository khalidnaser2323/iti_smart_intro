import 'package:bloc/bloc.dart';
import 'package:iti_smart_app/models/general_state.dart';
import 'package:iti_smart_app/models/user.dart';
import 'package:iti_smart_app/screens/users/users_view_model.dart';
import 'package:iti_smart_app/repos/users.dart';

class UsersListBloc extends Bloc<UsersListEvent, GeneralState<UsersListState>> {
  UsersListBloc() : super(GeneralState());

  @override
  Stream<GeneralState<UsersListState>> mapEventToState(
      UsersListEvent event) async* {
    if (event is GetAllUsers) {
      try {
        yield GeneralState(waiting: true);
        List<User> usersList = await UsersRepo().getUsers();
        GeneralState<UsersListState> newState = GeneralState(
            data: UsersListState(usersList: usersList), hasData: true);
        yield newState;
      } catch (error) {
        yield GeneralState(hasError: true, error: error.toString());
      }
    } else if (event is GetSingleUserEvent) {
      GeneralState<UsersListState> newState = GeneralState(
          data: UsersListState(usersList: state.data.usersList), hasData: true);
      newState.data.selectedUser = state.data.usersList
          .firstWhere((element) => element.id == event.selectedUserId);
      yield newState;
    }
  }
}
