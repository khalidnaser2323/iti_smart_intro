import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iti_smart_app/components/user_card.dart';
import 'package:iti_smart_app/models/general_state.dart';
import 'package:iti_smart_app/models/user.dart';
import 'package:iti_smart_app/screens/createUser/create_user_view.dart';
import 'package:iti_smart_app/screens/userDetails/user_details_view.dart';
import 'package:iti_smart_app/screens/users/users_list_bloc.dart';
import 'package:iti_smart_app/screens/users/users_view_model.dart';

class UsersListView extends StatefulWidget {
  @override
  _UsersListViewState createState() => _UsersListViewState();
}

class _UsersListViewState extends State<UsersListView> {
  UsersListBloc usersListBloc;
  @override
  void initState() {
    super.initState();
    usersListBloc = UsersListBloc();
    usersListBloc.add(GetAllUsers());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Users"),
      ),
      body: _getUsersGridWidget(),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          // User newUser = await Navigator.push(context,
          //     MaterialPageRoute(builder: (context) => CreateUserView()));
          // if (newUser != null) {
          //   setState(() {
          //     _usersList.add(newUser);
          //   });
          // }
        },
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _getUsersListWidget() {
    // return ListView.separated(
    //     itemCount: _usersList.length,
    //     separatorBuilder: (context, index) => Divider(),
    //     itemBuilder: (BuildContext context, int index) {
    //       return UserCard(
    //         user: _usersList[index],
    //         onItemClicked: (User clickedUser) {
    //           print("Clicked user ${clickedUser.name}");
    //         },
    //       );
    //     });
  }

  Widget _getUsersGridWidget() {
    return BlocBuilder<UsersListBloc, GeneralState<UsersListState>>(
        cubit: usersListBloc,
        builder: (context, state) {
          if (state.hasData) {
            return GridView.count(
                crossAxisCount: 2,
                padding: EdgeInsets.all(12.0),
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 10.0,
                children: List.generate(state.data.usersList.length, (index) {
                  return UserCard(
                    user: state.data.usersList[index],
                    displayAsCard: true,
                    onItemClicked: (User clickedUser) async {
                      usersListBloc.add(GetSingleUserEvent(
                          selectedUserId: state.data.usersList[index].id));
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return BlocProvider.value(
                            value: usersListBloc,
                            child: UserDetailsView(),
                          );
                        },
                      ));
                    },
                  );
                }));
          } else if (state.hasError) {
            return Center(child: Text(state.error.toString()));
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}
