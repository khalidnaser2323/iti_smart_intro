import 'package:iti_smart_app/models/user.dart';

abstract class UsersListEvent {}

class GetAllUsers extends UsersListEvent {}

class GetSingleUserEvent extends UsersListEvent {
  int selectedUserId;
  GetSingleUserEvent({this.selectedUserId});
}

class UsersListState {
  List<User> usersList;
  User selectedUser;
  UsersListState({this.usersList, this.selectedUser});
}
