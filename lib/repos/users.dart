import 'package:iti_smart_app/handlers/services_handler.dart';
import 'package:iti_smart_app/models/httpError.dart';
import 'package:iti_smart_app/models/user.dart';

class UsersRepo {
  String endPoint = "/users";
  Future<List<User>> getUsers() async {
    try {
      dynamic response = await ServicesHandler().httpGet(endPoint);
      Iterable usersList = response['data'];
      List<User> users = usersList.map((user) => User.fromJson(user)).toList();
      return users;
    } on HttpError catch (e) {
      throw e.error;
    } catch (error) {
      print("Get users error");
      print(error);
      throw Exception("Error getting users list");
    }
  }
}
