import 'package:flutter/material.dart';
import 'package:iti_smart_app/models/user.dart';

class UserCard extends StatelessWidget {
  final User user;
  final Function onItemClicked;
  final bool displayAsCard;
  final Color backGroundColor;
  UserCard({
    this.user,
    this.onItemClicked,
    this.displayAsCard = false,
    this.backGroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return this.displayAsCard ? _getUserCard() : _getUserAsListTile();
  }

  Widget _getUserAsListTile() {
    return ListTile(
      tileColor: this.backGroundColor,
      onTap: () {
        this.onItemClicked(user);
      },
      leading: FadeInImage.assetNetwork(
        placeholder: "assets/images/placeholder.png",
        image: user.image,
      ),
      title: Text(user.name),
      subtitle: Text(
        user.email,
      ),
      trailing: Icon(Icons.arrow_forward),
    );
  }

  Widget _getUserCard() {
    return InkWell(
      onTap: () {
        onItemClicked(user);
      },
      child: Card(
        color: backGroundColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FadeInImage.assetNetwork(
              placeholder: "assets/images/placeholder.png",
              image: user.image,
              width: 75.0,
              height: 75.0,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.0,
                bottom: 2.0,
              ),
              child: Text(
                user.name,
                style: TextStyle(
                  fontSize: 14.0,
                ),
              ),
            ),
            Text(
              user.email,
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
