import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:iti_smart_app/screens/home/home_view.dart';
import 'package:iti_smart_app/screens/userDetails/user_details_view.dart';
import 'package:iti_smart_app/screens/userImage/user_image_view.dart';
import 'package:iti_smart_app/screens/users/users_list_view.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return MaterialApp(
            home: UserImageView(),
            debugShowCheckedModeBanner: false,
          );
        } else {
          return Container();
        }
      },
    );
  }
}
